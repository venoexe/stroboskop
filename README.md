# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
 git clone https://venoexe@bitbucket.org/venoexe/stroboskop.git
```

Naloga 6.2.3:

https://bitbucket.org/venoexe/stroboskop/commits/19042f72e53856053d7ec4fe8161115ccf273d2a

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/venoexe/stroboskop/commits/d9e9de6d93f830cb83331ddf9bc9cc7923df50c5

Naloga 6.3.2:
https://bitbucket.org/venoexe/stroboskop/commits/2a6c19ce43849b0cf3dc9d5f198fb823dec038a7

Naloga 6.3.3:
https://bitbucket.org/venoexe/stroboskop/commits/ba1809c185f513c056574d6ae4d0d720de791a9e

Naloga 6.3.4:
https://bitbucket.org/venoexe/stroboskop/commits/21227c455e067108ce058a5db2997f81a58a0374

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/venoexe/stroboskop/commits/0b9ff55b631f072ad76abaed74d4e8b9001c1f8f

Naloga 6.4.2:
https://bitbucket.org/venoexe/stroboskop/commits/f608f05c99a136e26329949cbcd21d28d2bec2bb

Naloga 6.4.3:
https://bitbucket.org/venoexe/stroboskop/commits/b366756cfd66af46d9250b08cf0c3ab18544e371

Naloga 6.4.4:
https://bitbucket.org/venoexe/stroboskop/commits/e378511ae8e5166b11337bb0a5c284d21dc14488